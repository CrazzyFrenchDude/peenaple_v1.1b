**KNOWLEDGE /ˈnɒlɪdʒ/** facts, information, and skills acquired through experience or education; the theoretical or practical understanding of a subject.
Implying to **SHARE**

# PeeNaPle_V1.1b

PeeNaPle is a modular controller BoB for [Teensy 4.1](https://www.pjrc.com/store/teensy41.html) from PJRC. It is designed with Pick and place machines using Open PnP software in mind, but is also 3D Printing, CNC and other Cartesian machines oriented, thanks to an extension and adapter daughter boards eco-system being developed. It is built upon a 6 layer PCB, offering stability an immunity to EMF and electric noises. it Natively offers 8 stepper outputs, 4 End_Stops, UEXT port, Extension port, USB_HOST, Ethernet PHY and CAN_FD. Among the adaptors and extensions being developed you can find a stepper driver adapter to use your favorite TMC2208/09 or else as well as a powerful driver with closed loop so you never miss a step, capable of handling not less than 10A, just to name those 2! Tolerance of up to 35V Max Power In and offers 12V, 5V 3A DCDC, and 3.3V 1A for your convenience. Which are available on the extension ports so you do not need complicated wiring!

You can follow my projects @crazzyfrenchdude on Instagram

Website and store coming soon @ www.crazzyfrenchdude.com



**Revision 1.2b**

1. Added CAN_BUS termination with a 3 pin header to keep it Open or Closed! Thanks to "Eklun"
2. Have updated gerber files and schematics
3. BOM splited in 2 THT and SMD
4. Front pcb 3D pict updated


                    
**Revision 1.3b**                    

1. Added a 4 pins header for more A/D I/O flexibility, thanks again to "Eklun" closly reviewing the design!
2. Schematics and Gerber files update conciquently!
3. Front rendered design updated as well



**Revision 1.4b**

1. X axis DIR and STP swap correction
2. Few Pin assignment numbers correction